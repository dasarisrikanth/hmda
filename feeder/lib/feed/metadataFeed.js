// metadatafeed.js
const redis = require('redis');
const metadataFetch = require('./../fetcher/metadata');
const metdataProcessor = require('./../processor/processMetadata');
const rclient = require('./../redisclient');
const constants = require('./../constants');
const _ = require('lodash');
const progress = require('./../core/progress');
const stringifyWithKeys = require('./../core/common').stringifyWithKeys;

const metadata = metadataFetch(); 

module.exports = function(){
    metadata.then(d => {
        _.values(constants.connceptId).forEach(conceptId => {
            console.log('Processing the concept :' + conceptId);
            const data = metdataProcessor(d,conceptId);
            progress.init(data.length);
            let counter=1;
            data.forEach(element => {                
                
                // let queryStr = `Create (${conceptId}_${element._id}:metadata${stringifyWithKeys(element)})`
                let queryStr = `Create (:${conceptId}${stringifyWithKeys(element)})`
                
                rclient.setGraphQuery(queryStr)
                .then(res=>{
                    	if(res) {
                            // console.log(res.getStatistics());                    
                            progress.update(counter++)
                        }
                })
                .catch(e=>{
                    console.log(e);
                });
            });
            progress.stop()
        });
    });
} 

