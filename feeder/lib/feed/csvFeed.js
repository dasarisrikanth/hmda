// csvFeed.js

const redis = require('redis');
const rclient = require('../redisclient');
const fs = require('fs');
const fastcsv = require('fast-csv');
const progress = require('./../core/progress');
const logger = require('./../core/logger');

// Credits : https://techoverflow.net/2012/09/16/how-to-get-filesize-in-node-js/
function getFilesizeInBytes(filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}


// Credits : https://stackoverflow.com/questions/1248302/how-to-get-the-size-of-a-javascript-object
function roughSizeOfObject( object ) {
    var objectList = [];
    var stack = [ object ];
    var bytes = 0;

    while ( stack.length ) {
        var value = stack.pop();

        if ( typeof value === 'boolean' ) {
            bytes += 4;
        }
        else if ( typeof value === 'string' ) {
            bytes += value.length * 2;
        }
        else if ( typeof value === 'number' ) {
            bytes += 8;
        }
        else if
        (
            typeof value === 'object'
            && objectList.indexOf( value ) === -1
        )
        {
            objectList.push( value );

            for( var i in value ) {
                stack.push( value[ i ] );
            }
        }
    }
    return bytes;
}


module.exports = function(path, keyname) {

    console.log("Processing the feed:" + keyname)
    
    let readableStreamInput = fs.createReadStream(path);
    let i = 1;
    let processedBytes = 0;
    progress.init(getFilesizeInBytes(path));
    
    fastcsv.fromStream(readableStreamInput,{headers:true})
        .on('data',(data) => {
            let rowData = {};
            
            Object.keys(data).forEach(currentKey => {
                rowData[currentKey] = data[currentKey]
            });
            
            processedBytes += roughSizeOfObject(rowData);
            
            rclient.setHashSet('hmda:'+ keyname +':' + i++ , rowData, progress.update(processedBytes));
            
        })
        .on('end',()=>{
            console.log('Feed reading is end');
            loggger.debug( keyname +"Feed Reading is end");
            progress.stop();
        });

} 
