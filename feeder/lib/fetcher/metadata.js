// metadata.js
// retrieves the basic metadata info for HMDA
const fetch = require('isomorphic-fetch');
const constants = require('./../constants');

async function fetchMetadata() {
    const url = constants.endpoints.baseUrl + constants.endpoints.metadataEndpoint;
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

module.exports = fetchMetadata;