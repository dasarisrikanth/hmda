// processMetadata.js
// it process the metadata

const _ = require('lodash');

module.exports = function(data,conceptId) {
    const obj = _.find(_.get(data, "_embedded.concepts"),(p)=>{
        return p.id === conceptId
    });
    if(!_.isUndefined(obj)) {
        if(!_.isUndefined(obj.table)) {
            if(!_.isUndefined(obj.table.data)) {
                return obj.table.data;
            }
        }
    }
    return undefined;
}

