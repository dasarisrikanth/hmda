const _cliProgress = require('cli-progress');

const bar1 = new _cliProgress.Bar({
    barCompleteChar: '#',
    barIncompleteChar: '.',
    fps: 5,
    stream: process.stdout,
},_cliProgress.Presets.legacy);    

module.exports.init = (size) => {
    bar1.start(size,0);
}

module.exports.update = (counter) => {
    bar1.update(counter);    
}

module.exports.stop = () =>{
    bar1.stop()
}
