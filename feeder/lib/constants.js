// Constants.js

module.exports.redis = {
    redisHost:process.env.IS_FROM_DOCKER ? 'redis':'192.168.99.100',
    redisPort:6379,
    
};



module.exports.endpoints = {
    baseUrl:"https://api.consumerfinance.gov",
    metadataEndpoint:"/data/hmda.json",
    larEndpoint:"/data/hmda/slice/hmda_lar.json",
    institutionsEndpoint:"/data/hmda/slice/institutions.json",
    censusEndpoint:"/data/hmda/slice/census_tracts.json"
}

module.exports.conceptMapper = [
    {id:"co_applicant_ethnicity" , Name:"Co - Applicant Ethnicity", mappingId:"ethnicity"},
    {id:"purchaser_type" , Name:"Purchaser Type", mappingId:"purchaser_type"},
    {id:"respondent_id" , Name:"Respondent ID", mappingId:"respondent_id"},
    {id:"preapproval" , Name:"Preapproval", mappingId:"preapproval"},
    {id:"co_applicant_race_1" , Name:"Co - Applicant Race 1", mappingId:"race"},
    {id:"applicant_race_5" , Name:"Applicant Race 5", mappingId:"race"},
    {id:"msamd" , Name:"MSA/MD", mappingId:"msamd"},
    {id:"sex" , Name:"Sex", mappingId:"sex"}, // can be removed
    {id:"denial_reason_1" , Name:"Denial Reason 1", mappingId:"denial_reason"},
    {id:"co_applicant_race_2" , Name:"Co-Applicant Race 2", mappingId:"race"},
    {id:"applicant_race_4" , Name:"Applicant Race 4", mappingId:"race"},
    {id:"county_code" , Name:"County Code", mappingId:"county_code"},
    {id:"number_of_owner_occupied_units" , Name:"Number of Owner-Occupied Units", mappingId:"number_of_owner_occupied_units"},
    {id:"action_taken" , Name:"Action Taken", mappingId:"action_taken"},
    {id:"race" , Name:"Race", mappingId:"race"}, // can be removed
    {id:"agency_code" , Name:"Agency Code", mappingId:"agency_code"}, 
    {id:"loan_purpose" , Name:"Loan Purpose", mappingId:"loan_purpose"}, 
    {id:"application_date_indicator" , Name:"Application Date Indicator", mappingId:"application_date_indicator"},  // Need to analyse more
    {id:"edit_status" , Name:"Edit Status", mappingId:"edit_status"},
    {id:"property_type" , Name:"Property Type", mappingId:"property_type"},
    {id:"ethnicity" , Name:"Ethnicity", mappingId:"ethnicity"}, // Can be removed
    {id:"applicant_race_1" , Name:"Applicant Race 1", mappingId:"race"},
    {id:"applicant_race_3" , Name:"Applicant Race 3", mappingId:"race"}, 
    {id:"lien_status" , Name:"Lien Status", mappingId:"lien_status"}, 
    {id:"denial_reason_3" , Name:"Denial Reason 3", mappingId:"denial_reason"}, 
    {id:"denial_reason" , Name:"Denial Reason", mappingId:"denial_reason"}, // can be removed
    {id:"owner_occupancy" , Name:"Owner Occupancy", mappingId:"owner_occupancy"}, 
    {id:"co_applicant_race_4" , Name:"Co-Applicant Race 4", mappingId:"race"}, 
    {id:"co_applicant_race_5" , Name:"Co-Applicant Race 5", mappingId:"race"}, 
    {id:"co_applicant_race_3" , Name:"Co-Applicant Race 3", mappingId:"race"}, 
    {id:"applicant_sex" , Name:"Applicant Sex", mappingId:"sex"}, 
    {id:"applicant_race_2" , Name:"Applicant Race 2", mappingId:"race"},
    {id:"hoepa_status" , Name:"HOEPA Status", mappingId:"hoepa_status"}, 
    {id:"denial_reason_2" , Name:"Denial Reason 2", mappingId:"denial_reason"},
    {id:"loan_type" , Name:"Loan Type", mappingId:"loan_type"}, 
    {id:"applicant_ethnicity" , Name:"Applicant Ethnicity", mappingId:"ethnicity"}, 
    {id:"state_code" , Name:"State Code", mappingId:"state_code"}, 
    {id:"co_applicant_sex" , Name:"Co-Applicant Sex", mappingId:"sex"},
    {id:"fips" , Name:"County", mappingId:"fips"}, 
]

module.exports.connceptId = {
    stateCode:"state_code",
    loanType:"loan_type",
    fips:"fips",
    hoepaStatus:'hoepa_status',
    ownerOccupancy:'owner_occupancy',
    denialReason:'denial_reason',
    lienStatus:'lien_status',
    ethnicity:'ethnicity',
    propertytype:'property_type',
    editStatus:'edit_status',
    loanPurpose:'loan_purpose',
    agencyCode:'agency_code',
    race:'race',
    actionTaken:'action_taken',
    sex:'sex',
    msamd:'msamd',
    preapproval:'preapproval',
    purchaserType:'purchaser_type'
}
