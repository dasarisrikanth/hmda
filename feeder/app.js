const metadataFeed = require('./lib/feed/metadataFeed');
const institutionsFeed = require('./lib/feed/csvFeed');
const rclient = require('./lib/redisclient');


// Initilize redis Client
rclient.initRedisClient("hmda");


// rclient.setGraphQuery("MotoGP","MATCH (r:Rider)-[:rides]->(t:Team) WHERE t.name = 'Yamaha' RETURN r,t").then(c=>{
//     console.log(c);
//     console.log(c.next().getString("r.name"));
// });

// rclient.setGraphQuery("Social","CREATE (:person {name: 'roi', age: 33, gender: 'male', status: 'married'})").then(c=>{
//     console.log(c);
// });

// rclient.setGraphQuery("Social","MATCH (a:person) RETURN a").then(res=>{
//     while(res.hasNext()){
// 		let record = res.next();
// 		console.log(record.getString('a.name'));
// 	}
// });

metadataFeed();

// For Temparary
// institutionsFeed('./data/institutions.csv','institutions');
// institutionsFeed('./data/census_tracts.csv','census_tracts');

