import { ActionTypes as types} from '../constants/actionTypes';

export default (state = {}, action) => {
    switch(action.type) {
        case types.RECEIVED_PING_SUCCESS:
            return {
                ...state,
                ping:action.payload
            };
        case types.RECEIVED_PING_ERROR:
            return {
                ...state,
                error:action.error                
            }
        default:
            return state;
    }
}