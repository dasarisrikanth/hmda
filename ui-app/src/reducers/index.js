import { combineReducers } from 'redux';
import common from './common';
import services from './services';
import ping from './ping';

export default combineReducers({
    common,
    services,
    ping    
});
