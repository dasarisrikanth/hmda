import { ActionTypes as types} from '../constants/actionTypes';

const defaultState = {
    appName: 'HMDA',
    appLoaded:false, 
    tabCollection: []
};

export default (state = defaultState, action) => {
    switch(action.type) {
        case types.APP_LOAD: 
            return {
                ...state,
                appLoaded:true
            };
        case types.ADD_TAB: 
            return {
                ...state,
                tabCollection:[...state.tabCollection, action.payload]
            };
        default:
            return state;
    }
}