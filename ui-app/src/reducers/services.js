import { ActionTypes as types} from '../constants/actionTypes';

const defaultState = {
    metadata:[]
};

export default (state = defaultState, action) => {
    switch(action.type) {
        case types.RECEIVED_METADATA_SUCCESS:
            return {
                ...state,
                metadata:action.payload
            };
        case types.RECEIVED_METADATA_ERROR:
            return {
                ...state,
                error:action.error                
            }
        default:
            return state;
    }
}