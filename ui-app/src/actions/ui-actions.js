import axios from 'axios';
import debounce from 'lodash.debounce';
import { ActionTypes as types} from '../constants/actionTypes';
import {baseUrl, pingEndpoint} from '../constants/settings';


export function appLoaded() {
    return {
        type:types.APP_LOAD
    }
}

export function addTab(tabObj) {
    return(dispatch) => {
        dispatch({type:types.ADD_TAB,payload:tabObj});
    }    
}

export function fetchPing() {
    return(dispatch) => {
        makePingAjaxCall(dispatch);
    };
}

function _makePingAjaxCall(dispatch) {
    dispatch({type:types.REQUEST_PING, payload:{}});

    axios.get(`${baseUrl}${pingEndpoint}`)
        .then((resp)=>{
            dispatch({type:types.RECEIVED_PING_SUCCESS, payload: resp.data});
        })
        .catch((err)=>{
            dispatch({type:types.RECEIVED_PING_ERROR, error:err});
        })
}

const  makePingAjaxCall = debounce(_makePingAjaxCall,300);