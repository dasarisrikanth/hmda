import axios from 'axios';
import debounce from 'lodash.debounce';
import { ActionTypes as types} from '../constants/actionTypes';
import {graphQlEndpoint} from '../constants/settings';
import * as _ from "lodash";
export function fetchMetataData() {
    return(dispatch) => {
        makeMetadataAjaxCall(dispatch);
    }
}

const reqMetadata = {
    query: `
    query {
        metadata{
            conceptId,
            conceptName,
            conceptData {
                name
                code
                _id
                abbr
                county_name
                state_abbr
                year
            }
        }
    }   
    `
}

function _makeMetadataAjaxCall(dispatch) {
    dispatch({type:types.REQUEST_METADATA, payload:{}});

    axios.post(graphQlEndpoint,JSON.stringify(reqMetadata),{ headers:{'Content-Type':'application/json'}})
        .then((resp)=>{
            dispatch({type:types.RECEIVED_METADATA_SUCCESS, payload:  _.get(resp.data,"data.metadata",{})});
        })
        .catch((err)=>{
            dispatch({type:types.RECEIVED_METADATA_ERROR, error:err});
        })
}

const  makeMetadataAjaxCall = debounce(_makeMetadataAjaxCall,300);