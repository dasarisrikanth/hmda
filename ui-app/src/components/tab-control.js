import * as React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles
} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import * as _ from "lodash";
import TabPage from "./tab-page";
import Layout from './layout';
import './../../node_modules/react-grid-layout/css/styles.css';
import './../../node_modules/react-resizable/css/styles.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
        // backgroundColor: theme.palette.background.paper,
    },
    tabsRoot: {
        borderBottom: '1px solid #e8e8e8',
    },
    tabsIndicator: {
        backgroundColor: '#1890ff',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: 72,
        fontWeight: theme.typography.fontWeightRegular,
        marginRight: theme.spacing.unit * 4,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            color: '#40a9ff',
            opacity: 1,
        },
        '&$tabSelected': {
            color: '#1890ff',
            fontWeight: theme.typography.fontWeightMedium,
        },
        '&:focus': {
            color: '#40a9ff',
        },
    },
    tabSelected: {},
    typography: {
        padding: theme.spacing.unit * 3,
    },
});

class TabControl extends React.Component {

    state = {
        value: 0,
        layout: [] 
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    onLayoutChange = layout => {
        this.setState({ layout: layout });
    };

    stringifyLayout() {
        return this.state.layout.map(function(l) {
        return (
            <div className="layoutItem" key={l.i}>
            <b>{l.i}</b>: [{l.x}, {l.y}, {l.w}, {l.h}]
            </div>
        );
        });
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <div className={classes.root}>
                <Tabs
                value={value}
                onChange={this.handleChange}
                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                >
                {_.isObject(this.props.tabCollection) 
                ? 
                this.props.tabCollection.map(x=><Tab
                                                key={x.conceptData.label}
                                                disableRipple
                                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                                label={x.conceptData.label}
                                                />)
                :
                <div>&nbsp;</div>}
                </Tabs>
                <TabPage>
                    <Layout />
                </TabPage>
            </div>
        )
    }
}

TabControl.propTypes = {
    classes: PropTypes.object.isRequired,
    tabcollection: PropTypes.array
};
export default withStyles(styles)(TabControl);