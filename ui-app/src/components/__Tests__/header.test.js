import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Header from '../header'
import { AppBar, Toolbar, Typography } from '@material-ui/core';
const propData = {
    appName:"test app"
}

describe("Given Header Component..",() => {
    it('Then Header component should exists..', ()=> expect(Header).toBeDefined());    
});

describe("Given Default Values..", () => {
    let shallow;

    beforeEach(()=>{
        shallow = createShallow();
    });


    it('renders properly',() => {
        const wrapper = shallow(<Header appName={propData.appName} />)
        expect(wrapper).not.toBe(null);
        expect(wrapper.dive().find(AppBar)).toHaveLength(1);
        expect(wrapper.dive().find(Toolbar)).toHaveLength(1);
        expect(wrapper.dive().find(Typography)).toHaveLength(1);
    });
});
