import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import DropDown from '../dropdown';
import {FormControl, InputLabel, Select, MenuItem} from '@material-ui/core';


describe("Given Dropdown Component..",() => {
    it('Then Dropdown component should exists..', ()=> expect(DropDown).toBeDefined());    
});

describe("Given Default Values..", () => {
    let shallow;
    const propData = {
        data: ["test1","test2","test3"],
        label:"concept",
        handleChange:jest.fn()
    };
    beforeEach(()=>{
        shallow = createShallow();
    });

    it('renders properly',() => {
        const wrapper = shallow(<DropDown data={propData.data}  label={propData.label}/>)
        expect(wrapper).not.toBe(null);
        expect(wrapper.dive().find(FormControl)).toHaveLength(1);
        expect(wrapper.dive().find(InputLabel)).toHaveLength(1);
        expect(wrapper.dive().find(Select)).toHaveLength(1);
        expect(wrapper.dive().find('option')).toHaveLength(4);
    });

    it('calls a function when it changes the selection',()=>{
        const mountDropdownWithCallback = shallow(<DropDown handleChange={propData.handleChange} data={propData.data}  label={propData.label}/>);
        mountDropdownWithCallback.dive().find(Select).at(0).simulate("change",{target:{value:"test3"}});
        expect(propData.handleChange).toHaveBeenCalled();
        expect(propData.handleChange.mock.calls.length).toEqual(1);
    });

});


describe('when null data passes to dropdown', ()=>{
    let shallow;
    const propData = {
        data: null,
        label:"concept",
        handleChange:jest.fn()
    };

    beforeEach(()=>{
        shallow = createShallow();
    });

    it('renders the dropdown with out crash',()=>{
        const wrapper = shallow(<DropDown data={propData.data}  label={propData.label}/>)
        expect(wrapper).not.toBe(null);
        expect(wrapper.dive().find(FormControl)).toHaveLength(1);
        expect(wrapper.dive().find(InputLabel)).toHaveLength(1);
        expect(wrapper.dive().find(Select)).toHaveLength(1);
        expect(wrapper.dive().find('option')).toHaveLength(1);
    });
});
