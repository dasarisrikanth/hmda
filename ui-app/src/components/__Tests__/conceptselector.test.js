import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import ConceptSelector from '../concept-selector';
import AutoSelect from '../autoselect';

const propData = {
    metaData:[]
}

describe("Given Header Component..",() => {
    it('Then Header component should exists..', ()=> expect(ConceptSelector).toBeDefined());    
});

describe("Given Default Values..", () => {
    let shallow;

    beforeEach(()=>{
        shallow = createShallow();
    });

    it('renders properly',() => {
        const wrapper = shallow(<ConceptSelector metadata={propData.metaData}  />)
        expect(wrapper).not.toBe(null);
        expect(wrapper.dive().find(AutoSelect)).toHaveLength(1);        
    });
});
