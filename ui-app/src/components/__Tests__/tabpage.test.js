import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import TabPage from '../tab-page';

describe("Given Dropdown Component..",() => {
    it('Then Dropdown component should exists..', ()=> expect(TabPage).toBeDefined());    
});

// describe("Given Default Values..", () => {
//     let shallow;
//     const propData = {
//         data: ["test1","test2","test3"]        
//     };
//     beforeEach(()=>{
//         shallow = createShallow();
//     });

//     it('renders properly',() => {
//         const wrapper = shallow(<AutoSelect data={propData.data} />)
//         expect(wrapper).not.toBe(null);
//         expect(wrapper.dive().find(Select)).toHaveLength(1);        
//     });
    
// });


// describe('when null data passes to dropdown', ()=>{
//     let shallow;
//     const propData = {
//         data: null,
//         label:"concept",
//         handleChange:jest.fn()
//     };

//     beforeEach(()=>{
//         shallow = createShallow();
//     });

//     it('renders the dropdown with out crash',()=>{
//         const wrapper = shallow(<AutoSelect data={propData.data}  label={propData.label}/>)
//         expect(wrapper).not.toBe(null);
//         expect(wrapper.dive().find(Select)).toHaveLength(1);
//     });
// });
