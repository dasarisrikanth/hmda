import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import App from '../app'
import Adapter from './../../setupTests';
import ConceptSelector from '../concept-selector';
import Header from '../header';

import configureStore from 'redux-mock-store'

// create any initial state needed
const initialState = {
    appName: 'HMDA',
    appLoaded:false
}; 


// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();

describe("Given Header Component..",() => {
    it('Then Header component should exists..', ()=> expect(App).toBeDefined());    
});

describe("Given Default Values..", () => {
    let shallow;
    let store;
    beforeEach(()=>{
        store = mockStore(initialState);
        shallow = createShallow();
    });


    it('renders properly',() => {
        const wrapper = shallow(<App  store={store}  />)
        expect(wrapper).not.toBe(null);
    });
});
