import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

function TabPage(props) {
    return (
    <Typography component="div" >
        {props.children}
    </Typography>
    );
}


TabPage.propTypes = {
    children: PropTypes.node.isRequired,
};


export default TabPage;