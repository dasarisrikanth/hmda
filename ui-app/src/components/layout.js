import * as React from 'react';
import GridLayout from 'react-grid-layout';
import './../../node_modules/react-grid-layout/css/styles.css';
import './../../node_modules/react-resizable/css/styles.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
        flexGrow: 1,
    },
    widget : {
        border:1,
        background:'#eee'
    }
};

class Layout extends React.Component {
    render() {
    // layout is an array of objects, see the demo for more complete usage
    // var layout = [
    //     {i: 'a', x: 0, y: 0, w: 1, h: 2},
    //     {i: 'b', x: 1, y: 0, w: 3, h: 2, minW: 2, maxW: 4},
    //     {i: 'c', x: 4, y: 0, w: 1, h: 2}
    // ];
    return (
        <GridLayout className={this.props.classes.root} layout={this.props.layout} cols={12} rowHeight={30} width={1200}>
            <div className={this.props.classes.widget} key="a">a</div>
            <div className={this.props.classes.widget} key="b">b</div>
            <div className={this.props.classes.widget} key="c">c</div>
        </GridLayout>
        )
    }
}

Layout.propTypes = {
    classes: PropTypes.object.isRequired,
    layout: PropTypes.array
};

export default withStyles(styles)(Layout);