import React from 'react';
import {connect} from 'react-redux';
import Header from './header';
import compose from 'recompose/compose'
import withRoot from './theme';
import * as uiActions from '../actions/ui-actions';
import * as metadataActions from '../actions/metadata-action';
import ConceptSelector from './concept-selector';



class App extends React.Component {

    constructor(props) {
        super(props);

        this.onTabCollectionChange = this.onTabCollectionChange.bind(this);
    }
    
    componentDidMount() {
        this.props.dispatch(uiActions.appLoaded());
        this.props.dispatch(uiActions.fetchPing());
        this.props.dispatch(metadataActions.fetchMetataData());
    }

    onTabCollectionChange = (obj) => {
        this.props.dispatch(uiActions.addTab(obj));
    }

    render() {
        if(this.props.appLoaded) {
            return(
                <div>
                    <Header {...this.props} />
                    <ConceptSelector {...this.props} dispatchTabCollection={this.onTabCollectionChange} />
                </div>
            );
        }

        return(
            <div>
                <h1>loading...</h1>
            </div>
        )
    }

}


export default compose(
    withRoot,
    connect((state, props) => { 
        return { 
            appLoaded: state.common.appLoaded,
            appName: state.common.appName,
            metadata: state.services.metadata,
            tabCollection: state.common.tabCollection
        }
    })
)(App);