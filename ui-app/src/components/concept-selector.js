import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles
} from '@material-ui/core/styles';
import * as _ from "lodash";
import AutoSelect from './autoselect';
import TabControl from './tab-control';
const styles = {
    root:{
        flexGrow:1
    },
    searchroot: {
        display:'flex',
        width:'70%',
        margin:'0 auto',
        maxHeight:'100px'
    }, 
    concept: {
        width:'250px'
    },
    conceptData: {
        flex:1
    },
    tabContainer: {
        width:'100%',
        flexGrow:1,
        clear: 'both',
        float: 'left',
        display: 'block',
        position: 'relative'
    }
};

class ConceptSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedConcept: '',
            dataForConcept: [],
            selectedConceptData: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleDataCahnge = this.handleDataCahnge.bind(this);
    }
    
    handleChange = obj => {

        this.setState({
            selectedConceptData:null,
            selectedConcept: obj.value,
            dataForConcept:_.map(_.find(this.props.metadata, (o)=> {
                return (o.conceptId === obj.value)
            }).conceptData,(r)=>{
                return {
                    value : r._id,
                    label:r.name
                }
            })
        });
    };

    handleDataCahnge = obj => {
        this.setState({
            selectedConceptData: obj.value
        });

        this.props.dispatchTabCollection({
            concept:_.find(this.props.metadata, (o)=> { return (o.conceptId === this.state.selectedConcept)}),
            conceptData: obj
        });
    }

    render() {
        const data = _.map(this.props.metadata,(o)=> {
            return {
                value: o.conceptId,
                label: o.conceptName
            };
        });
        return (
            <div className={this.props.classes.root}>
                <div className={this.props.classes.searchroot}>
                    <div className={this.props.classes.concept}>
                        {/* <DropDown handleChange={this.handleChange} data={data} label="Concept" selectedConcept={this.state.selectedConcept} /> */}
                        <AutoSelect handleChange={this.handleChange} data={data} isSearchable={false} label="Select the Concept" selectedConcept={this.state.selectedConcept} />
                    </div>
                    <div className={this.props.classes.conceptData}>
                        {/* <DropDown handleChange={this.handleDataCahnge} data={this.state.dataForConcept} label=" " selectedConcept={this.state.selectedConceptData} /> */}
                        <AutoSelect handleChange={this.handleDataCahnge} data={this.state.dataForConcept} isSearchable={true} label="Search / Select the Concept Data" selectedConcept={this.state.selectedConceptData} />
                    </div>     
                </div>
                <div className={this.props.classes.tabContainer}>
                    <TabControl tabCollection={this.props.tabCollection} />
                </div>
            </div>
        );
    }
}

ConceptSelector.propTypes = {
    classes: PropTypes.object.isRequired,
    metadata: PropTypes.array,
    dispatchTabCollection: PropTypes.func
};

export default withStyles(styles)(ConceptSelector);

