import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FilledInput from '@material-ui/core/FilledInput';
import * as _ from "lodash";
const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',        
    },
    formControl: {
        margin: theme.spacing.unit,
        fullWidth:true,
        flexGrow: 1
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class DropDown extends React.Component {
    
    render() {
        const { classes } = this.props;
    
        return (
            <div className={classes.root}>
            <form  autoComplete="off">
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel htmlFor="filled-simple">{this.props.label}</InputLabel>
                    <Select
                        native
                        value={this.props.selectedConcept}
                        onChange={this.props.handleChange}
                        input={<FilledInput name="concept" id="filled-simple" />}
                    >
                        {/* <MenuItem value=""> 
                        <em>None</em>
                        </MenuItem>
                        {_.isObject(this.props.data) ? this.props.data.map(x=><MenuItem key={x._id} value={x._id}>{x.name}</MenuItem>) : ""} */}
                        <option key="" value="" />
                        {_.isObject(this.props.data) ? this.props.data.map(x=><option key={x._id} value={x._id}>{x.name}</option>) : ""} 
                    </Select>
                </FormControl>
            </form>            
            </div>
        );
    }

}

DropDown.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array,
    label: PropTypes.string,
    selectedConcept: PropTypes.string    
};


export default withStyles(styles)(DropDown);

