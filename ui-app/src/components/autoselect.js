import * as React from 'react';
import Select from  'react-select';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: 250,
        marginTop:10,
        marginBotton:10
    },
    input: {
        display: 'flex',
        padding: 0,
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    noOptionsMessage: {
        padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
    },
    singleValue: {
        fontSize: 16,
    },
    placeholder: {
        position: 'absolute',
        left: 2,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    }
});

class AutoSelect extends React.Component {
    render() {
        const { classes } = this.props;
        const selectStyles = {
            input: base => ({
                ...base,
                '& input': {
                    font: 'inherit',
                },
            }),
        };
        return (
            <div className={classes.root}>
                
                    <Select
                    classes={classes}
                    styles={selectStyles}
                    options={this.props.data}
                    placeholder={this.props.label}
                    defaultValue={this.props.selectedConcept}
                    onChange={this.props.handleChange}
                    isClearable={this.props.isSearchable}
                    isSearchable={this.props.isSearchable}
                    />
                
            </div>
        )

    }

}


AutoSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    metadata: PropTypes.array    
};

export default withStyles(styles)(AutoSelect);


