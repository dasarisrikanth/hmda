import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from './store/configureStore';

import App from './components/app';

const configuredStore = configureStore();

ReactDOM.render((
    <Provider store={configuredStore}>
        <App />
    </Provider>
), document.getElementById('root'));
