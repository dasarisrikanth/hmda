import keyMirror from 'keymirror';


export var ActionTypes = keyMirror ({
    //UI
    APP_LOAD:null,
    ADD_TAB:null,
    
    //AJAX Calls
    REQUEST_PING:null,
    RECEIVED_PING_SUCCESS:null,
    RECEIVED_PING_ERROR:null,
    //Metadata
    REQUEST_METADATA:null,
    RECEIVED_METADATA_SUCCESS:null,
    RECEIVED_METADATA_ERROR:null

});
