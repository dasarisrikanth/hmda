// export const baseUrl ="http://" + process.env.API_HOST +":3200";
export const baseUrl =`http://${process.env.REACT_APP_API_HOST}:3200`;

export const pingEndpoint = "/ping";
export const graphQlEndpoint = `${baseUrl}/graphql`;
// export const metadataEndoint = "/api/metadata";