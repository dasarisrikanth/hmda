import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './components/app';

const configuredStore = configureStore();

it('renders without crashing',()=>{
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={configuredStore}>
        <App />
    </Provider>,div);

    ReactDOM.unmountComponentAtNode(div);
});

