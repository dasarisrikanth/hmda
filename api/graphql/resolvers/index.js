const rclient = require('./../../lib/redisClient');
const _ = require('lodash');
const constants = require('./../../lib/constants');

module.exports = {
    metadataByCategory: (cat) =>{
        console.log("received "+cat);
        return rclient.setGraphQuery(`MATCH (n) where labels(n) = '${cat}' return distinct labels(n), n`)
                        .then(res=>{
                            let data=[];
                            while(res.hasNext()) {
                                let record = res.next();
                                let d={
                                    category: record.getString('labels(n)'),
                                    _id: record.getString('n._id'),
                                    abbr: record.getString('n.abbr'),
                                    name: record.getString('n.name'),
                                    county_name: record.getString('n.county_name'),
                                    code: record.getString('n.code'),
                                    state_abbr: record.getString('n.state_abbr'),
                                    year: record.getString('n.year')
                                };
                                data.push(d);
                            }
                            return data;
                        })
                        .then(d=>{
                            let finaldata = [];
                    
                            _.forEach(_.groupBy(d,'category'),(value,key)=>{
                                finaldata.push({"conceptName":key,"conceptData": value.map(y=>{
                                        return {
                                            _id: y._id,
                                            abbr: y.abbr,
                                            name: y.name,
                                            county_name: y.county_name,
                                            code: y.code,
                                            state_abbr: y.state_abbr,
                                            year: y.year
                                        };
                                    })
                                });
                            });
                            return finaldata;
                        })
                        .catch(err=>{
                            console.log(err);
                        });
                    
    },
    metadata: () => {
        return rclient.setGraphQuery("MATCH (n) return  distinct labels(n), n")
                .then(res=>{
                    let data=[];
                    // console.log(res);
                    while(res.hasNext()) {
                        let record = res.next();
                        let d={
                            category: record.getString('labels(n)'),
                            _id: record.getString('n._id'),
                            abbr: record.getString('n.abbr'),
                            name: record.getString('n.name'),
                            county_name: record.getString('n.county_name'),
                            code: record.getString('n.code'),
                            state_abbr: record.getString('n.state_abbr'),
                            year: record.getString('n.year')
                        };
                        data.push(d);
                    }
                    return data;
                })
                .then(d=>{
                    let finaldata = [];
            
                    _.forEach(_.groupBy(d,'category'),(value,key)=>{

                        finaldata.push({
                            "conceptId":key,
                            "conceptName":toTitleCase(key.replace("_"," ")),
                            "conceptData": value.map(y=>{
                                return {
                                    _id: y._id,
                                    abbr: y.abbr,
                                    name: y.name,
                                    county_name: y.county_name,
                                    code: y.code,
                                    state_abbr: y.state_abbr,
                                    year: y.year
                                };
                            })
                        });
                    });
                    return finaldata;
                })
                .catch(err=>{
                    console.log(err);
                });
    }
}


function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}
