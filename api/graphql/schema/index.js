const { buildSchema } = require('graphql');

module.exports = buildSchema(`
    type conceptObject {
        _id: ID!
        abbr: String
        name: String
        county_name: String
        code: String
        state_abbr: String
        year: String
    }

    type concepts {
        conceptId: String
        conceptName : String
        conceptData: [conceptObject]
    }

    type RootQuery {
        metadata: [concepts]
        metadataByCategory(cat: String!): concepts
    }

    schema {
        query: RootQuery
    }
`);