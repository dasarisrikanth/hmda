const rclient = require('./lib/redisClient');
const express = require('express');
const _ = require('lodash');
const bodyParser = require('body-parser');
const errorhandler = require('errorhandler');
const graphqlHttp = require('express-graphql'); 
const app = express();
const morgan = require('morgan')
const port = 3200;
const cors = require('cors');



const graphQlSchema = require('./graphql/schema/index')
const graphQlResolvers = require('./graphql/resolvers/index') 

// Initilizing 
rclient.initRedisClient("hmda");

var isProduction = process.env.NODE_ENV === 'production';

if (!isProduction) {
    app.use(errorhandler());
}


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(morgan('combined'));

var allowedOrigins = ['http://192.168.99.100:3000','http://127.0.0.1:3200','http://192.168.99.100:3200']
app.use(cors({
    origin:function(origin, callback) {
        // allow requests with no origin
        if(!origin) return callback(null, true);

        if(allowedOrigins.indexOf(origin) === -1) {
            var msg = 'This CORS policy for this site doesnt allow access from specified origin';
            return callback(new Error(msg), false);
        }
        console.log("cors passed");
        return callback(null, true);
    }
}));


app.get('/',function (req,res,next) {
    console.log("/" + req.method);
    res.end();
});

app.use('/graphql',graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql:true 
}));


app.get('/ping',function(req,res,next) {
    res.send("pong");
    res.end();
});

app.use('/',require('./routes'));

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (!isProduction) {
    app.use(function(err, req, res, next) {
    console.log(err.stack);
    res.status(err.status || 500);

    res.json({'errors': {
            message: err.message,
            error: err
        }});
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({'errors': {
        message: err.message,
        error: {}
    }});
});


var server = app.listen(port, function () {
    console.log('Server running at port :' + server.address().port);
});