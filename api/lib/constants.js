// Constants.js

module.exports.redis = {
    // redisHost:'172.17.0.2',
    redisHost:process.env.IS_FROM_DOCKER ? 'redis':'192.168.99.100',
    redisPort:6379,
    
};


module.exports.metadataConstants =[
    {mappingId:"race", name:"Co Applicant Race", grouped:["co_applicant_race_1","co_applicant_race_2","co_applicant_race_3","co_applicant_race_4","co_applicant_race_5"]},
    {mappingId:"race", name:"Applicant Race", grouped:["applicant_race_1","applicant_race_2","applicant_race_3","applicant_race_4","applicant_race_5"]},
    {mappingId:"ethnicity", name:"Co Applicant Ethnicity", grouped:["co_applicant_ethnicity"]},
    {mappingId:"ethnicity", name:"Applicant Ethnicity", grouped:["applicant_ethnicity"]},
    {mappingId:"sex", name:"Applicant Sex", grouped:["applicant_sex"]},
    {mappingId:"sex", name:"Co Applicant Sex", grouped:["co_applicant_sex"]},
    {mappingId:"denial_reason", name:"Denial Reason", grouped:["denial_reason","denial_reason_1","denial_reason_3","denial_reason_2"]},
    {mappingId:"purchaser_type", name:"Purchaser Type", grouped:["purchaser_type"]},
    {mappingId:"preapproval", name:"Preapproval", grouped:["preapproval"]},
    {mappingId:"action_taken", name:"Action Taken", grouped:["action_taken"]},
    {mappingId:"agency_code", name:"Agency Code", grouped:["agency_code"]},
    {mappingId:"loan_purpose", name:"Loan Purpose", grouped:["loan_purpose"]},
    {mappingId:"edit_status", name:"Edit Status", grouped:["edit_status"]},
    {mappingId:"property_type", name:"Property Type", grouped:["property_type"]},
    {mappingId:"owner_occupancy", name:"Owner Occupancy", grouped:["owner_occupancy"]},
    {mappingId:"loan_type", name:"Loan Type", grouped:["loan_type"]},
    {mappingId:"hoepa_status", name:"HOEPA Status", grouped:["hoepa_status"]},
    {mappingId:"lien_status", name:"Lien Status", grouped:["lien_status"]},
];

// module.exports.metadataMapper = [
    
//     {id:"msamd" , Name:"MSA/MD", mappingId:"msamd"},
//     {id:"county_code" , Name:"County Code", mappingId:"county_code"},
//     {id:"number_of_owner_occupied_units" , Name:"Number of Owner-Occupied Units", mappingId:"number_of_owner_occupied_units"},
    
    
//     // {id:"application_date_indicator" , Name:"Application Date Indicator", mappingId:"application_date_indicator"},  // Need to analyse more
    
    
//     {id:"state_code" , Name:"State Code", mappingId:"state_code"}, 
    
//     {id:"fips" , Name:"County", mappingId:"fips"}, 
// ]