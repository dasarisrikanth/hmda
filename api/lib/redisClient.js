// redisclient.js
const redis = require('redis');
const constants = require('./constants');
const bluebird = require('bluebird');
const logger = require('./logger');
const ResultSet = require('./resultSet');
// make node redis promise compatabile
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let redisClient = null;
let dbName = null;

module.exports = {
    initRedisClient : (db) => {
        console.log("redis host :" + constants.redis.redisHost);
        console.log("redis port :" + constants.redis.redisPort);
        redisClient = redis.createClient({
                host: constants.redis.redisHost,
                port: constants.redis.redisPort,
                retry_strategy: function (options) {
                    if (options.error && options.error.code === 'ECONNREFUSED') {
                        // End reconnecting on a specific error and flush all commands with
                        // a individual error
                        return new Error('The server refused the connection');
                    }
                    if (options.total_retry_time > 1000 * 60 * 60) {
                        // End reconnecting after a specific timeout and flush all commands
                        // with a individual error
                        return new Error('Retry time exhausted');
                    }
                    if (options.attempt > 10) {
                        // End reconnecting with built in error
                        console.log("10 attempts done..but no use");
                        return undefined;
                    }
                    // reconnect after
                    return Math.min(options.attempt * 100, 3000);
                }
            });
        
        logger.debug("Initilizing the Redis Client");

        redisClient.on('ready',function(){
            logger.debug('Redis is ready')
        });

        redisClient.on('connect', function(){
            logger.debug('Redis Client is connected');
            isRedisConnected = true;            
        });

        redisClient.on("message", function(channel, message) {
            logger.info("message recieved on channel :", channel);
            callback(channel,message);
        });

        redisClient.on("error", function (err) {
            logger.debug("Error occurred while connecting to redis " + err);
                    assert(error instanceof Error);
                    assert(error instanceof redis.AbortError);
                    assert(error instanceof redis.AggregateError);
                    // The set and get get aggregated in here
                    assert.strictEqual(error.errors.length, 2);
                    assert.strictEqual(error.code, 'NR_CLOSED');
            isRedisConnected = false;
        });

        dbName=db;
        // redis.add_command('graph.query');

    },



    getKeyValue: (key) => {
        return redisClient.getAsync(key)
            .then((res, err) => err ? Promise.reject("getKeyValue : "+err) : Promise.resolve(res));
    },

    setKeyValue: (key, value) => {
        return redisClient.setAsync(key, value)
            .then((res, err) => err ? Promise.reject("setkeyvalue : "+ err) : Promise.resolve(res));
    },

    doesKeyExist: key => {
        return redisClient.existsAsync(key)
            .then((res, err) => !res || err ? Promise.resolve(false) : Promise.resolve(res));
    },

    deleteKey: key => {
        return redisClient.delAsync(key)
            .then((res, err) => res ? Promise.resolve(res) : Promise.reject("deleteKey :"+err));
    },

    getAllKeys: (key) => {
        return redisClient.keysAsync(key)
            .then((res,err) => err ? Promise.reject("keys :"+err) : Promise.resolve(res));
    },

    getAllHashFields: (key) => {
        return redisClient.hgetallAsync(key)
            .then((res,err) => err ? Promise.reject('hmgetall :'+err): Promise.resolve(res));
    },

    setHashSet: (key, value, callback) => {
        logger.info("KEY :"+key +" , Value :"+value);
        return redisClient.hmsetAsync(key,value)
            .then((res,err) => err ? Promise.reject('HMSET :'+err): Promise.resolve(res))
            .catch(e=>{
                logger.error("Error :"+e);
            });
        callback();
    },

    publishMessage: (channel,message) => redisClient.publish(channel,message),
    
    endConnection: () => redisClient.quit(),

    subscribeChannel: (channel,cb) => {
        redisClient.subscribe(channel)
        callback = cb; 
    },

    setGraphQuery : (query) => {
        logger.info("Executing the "+query);
        return redisClient.send_commandAsync('graph.QUERY', [dbName,query])
                .then(res=>{
                    return Promise.resolve(new ResultSet(res));
                })
                .catch(e=>{
                    logger.error("Error :"+e);
                    Promise.reject('send_command :'+ e);
                });
    },
}