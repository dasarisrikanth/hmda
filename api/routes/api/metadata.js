const constants = require('./../../lib/constants');
const router = require('express').Router();
const rclient = require('./../../lib/redisClient');

router.get('/',async (req,res,next) => {
    await getMetadata()
    .then(x=>{
        res.status(200).json(x);
        res.end();
    })
    .catch(e=>{
        res.status(500).json({ error: e.toString() })
        res.end();
    });    
});

async function getMetadata() {
    let rootmetadata=[];

    for(const concept of constants.metadataConstants) {
        let data = await getKeysforConcept(concept);
        rootmetadata.push(data);
    }
    
    return rootmetadata;
}

async function getKeysforConcept(concept) {
    const concpetObj = concept;
    concpetObj.data = [];
    const res = await rclient.getAllKeys("hmda:metadata:"+concpetObj.mappingId+":*");
    for(const key of res) {
        concpetObj.data.push(await getdataforKeys(key));
    }
    return concpetObj;
}

async function getdataforKeys(key) {
    const data = await rclient.getAllHashFields(key);
    return data;
}

module.exports = router;